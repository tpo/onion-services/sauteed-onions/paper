# README

This repository contains the source code of our sauteed onions paper at WPES'22.

## Install

On a Debian system:

    # apt install texlive-full

## Build

    $ latexmk -pdf

The output file will be named `main.pdf`.

\section{Introduction} \label{sec:intro}
Onion addresses are domain names with many useful properties.  For example, an
onion address is self-authenticated due to encoding its own public key.  It
also makes integral use of the anonymity network Tor to provide secure and
private lookups as well as routing~\cite{tor-design}.  A major usability concern
is that onion addresses are random-looking strings; they are difficult to
discover, update, and remember~\cite{winter}.  Existing solutions approach these
limitations in different ways, e.g., ranging from setting onion addresses in
HTTP headers over HTTPS with so-called \emph{onion
location}~\cite{onion-location} and bookmarking found results to making use of
manually curated third-party
lists~\cite{onion-service-overview,h-e-securedrop,muffet-onions} as well as
search engines like DuckDuckGo or \texttt{ahmia.fi}~\cite{nurmi,winter}.

Herein we refer to the unidirectional association from a domain name to an onion
address as an \emph{onion association}.  The overall goal is to facilitate
transparent discovery of onion associations.  To achieve this we rely on the
observation that today's onion location can be implemented in certificates
issued by Certificate Authorities (CAs).  This is not an additional dependency
because onion location already requires HTTPS~\cite{onion-location}.  The main
benefit of transitioning from HTTP headers to TLS certificates is that all such
onion associations become signed and sequenced in tamper-evident Certificate
Transparency (CT) logs~\cite{ct/a,ct-rfc}, further tightening the relation
between CAs and onion keys~\cite{cab-onion-dv,cab-ballot144,secdev19} as well as
public CT logging and Tor~\cite{ctor-popets,muffet-onions}.

Our first contribution is to make onion associations identical for all Tor
users, and otherwise the possibility of inconsistencies becomes public via CT.
Consistency of available onion associations mitigates the threat of users
being partitioned without anyone noticing into subsets according to which
onion address they received during onion association.
Our second contribution is to construct a search engine that allows Tor users to
look up onion associations without having to trust the service provider
completely.  Other than being helpful to validate onion addresses as
authentic~\cite{winter}, such discovery can continue to work \emph{after} a TLS
site becomes censored.

Section~\ref{sec:preliminaries} briefly covers CT preliminaries.
Section~\ref{sec:trans} describes \emph{sauteed onions}, an approach that makes
discovery of onion associations more transparent and censorship-resistant
compared to today.  Section~\ref{sec:related} discusses related work.
Section~\ref{sec:conclusion} concludes the paper.
Appendix~\ref{app:search} contains query examples for our search engine.
Appendix~\ref{app:setup} outlines an example setup.
All artifacts are online~\cite{sauteed-onion-artifacts}.

%%% Local Variables: 
%%% mode: latex 
%%% TeX-master: "../main"
%%% End:          
